package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");

        flush(em -> {
            childEntity.setParentEntity(parentEntity);
            parentEntityRepository.save(parentEntity);
            childEntityRepository.save(childEntity);
        });
        List<ChildEntity> childEntityList = childEntityRepository.findAll();
        ChildEntity fetchedChild = childEntityList.get(0);
        assertEquals("parent", fetchedChild.getParentEntity().getName());

        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");

        flush(em -> {
            childEntity.setParentEntity(parentEntity);
            parentEntityRepository.save(parentEntity);
            childEntityRepository.save(childEntity);
        });

        flushAndClear(em -> childEntity.setParentEntity(null));

        List<ChildEntity> childEntityList = childEntityRepository.findAll();
        ChildEntity fetchedChild = childEntityList.get(0);
        assertNull(fetchedChild.getParentEntity());
        Optional<ParentEntity> parentEntityOptional = parentEntityRepository.findById(parentEntity.getId());
        assertTrue(parentEntityOptional.isPresent());
        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ParentEntity parentEntity = new ParentEntity("parent");
        ChildEntity childEntity = new ChildEntity("child");

        flush(em -> {
            childEntity.setParentEntity(parentEntity);
            parentEntityRepository.save(parentEntity);
            childEntityRepository.save(childEntity);
        });

        flushAndClear(em -> {
            childEntityRepository.delete(childEntity);
            parentEntityRepository.delete(parentEntity);
        });

        List<ParentEntity> parentEntities = parentEntityRepository.findAll();
        List<ChildEntity> childEntities = childEntityRepository.findAll();
        assertEquals(0, parentEntities.size());
        assertEquals(0, childEntities.size());

        // --end-->
    }
}